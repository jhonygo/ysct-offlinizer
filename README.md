# OFFLINIZER COMPONENTS FOR YSCT  
  
# BACKEND  
  
### RPMs (FROM A CENTOS BASE)  
RPM DRYRUN+INSTALL FROM A CONTAINER "backend_offline"  
```  
docker run --name backend_offline -it centos:centos7 bash
for package in epel-release python34 python34-devel gcc
do 
  yum install --downloadonly --downloaddir=/tmp/backend_rpms -y $package 
  yum install -y $package
done
```  
RPMs RETRIEVAL FROM OUTSIDE THE RUNNING CONTAINER "backend_offline" 
```  
docker cp backend_offline:/tmp/backend_rpms .  
```  
  
### PIP INSTALLER (FROM A UNIX BOX)  
PIP PACKAGES DOWNLOAD FROM A CENTOS:CENTOS7 CONTAINER WITH PYTHON INSTALLED, SUCH AS "backend_offline"  
```  
curl https://bootstrap.pypa.io/get-pip.py > get-pip.py  
mkdir /tmp/pip_installer ; cd   
chmod +x get-pip.py  
./get-pip.py --download pip_installer pip setuptools wheel  
```  
PIP RETRIEVAL FROM OUTSIDE THE RUNNING CONTAINER "backend_offline"  
```  
docker cp backend_offline:/tmp/pip_installer .  
```  
  
### VENV LIBRARIES (FROM THE RUNNING BACKEND CONTAINER NAMED ysct_backend_staging)  
```  
docker exec ysct_backend_staging tar -xzf /tmp/ysct/venv.tgz /tmp/ysct/venv  
docker cp ysct_backend_staging:/tmp/ysct/venv.tgz .  
```  
